# (d,eval)(crypt)

This tool allows one to write a snippet of code, encrypt it, and then send it to another user to decrypt.

This tool uses CryptoJS for {en,de}cryption, and uses base65536 for *some level* of compression. (I have not compared b65k against other compression algorithms)

On top of that, all of these operations are done client-side, encryption, decryption, end evaluation.

The code is not evaluated automatically, to give the user time to review the code for any malicious behavior.

There is a public instance hosted at [d-eval-crypt.surge.sh](https://d-eval-crypt.surge.sh)

Currently, this project will only work on browsers that support ES6 modules.

## Running

0. Install `yarn`
0. `yarn install`
0. `rollup -c rollup.config.js`
0. Serve using your preferred HTTP server. (`serve ./public`)

## Usage

0. Visit the page.
0. Enter key into `Key` input field.
0. Enter text into the large `textarea`.
0. Click `Eval` to run your code.
0. Refresh page and enter your key to see the auto-decrypting behavior.

## Contributing

Feel free to submit any pull requests, or open any issues regarding this project!

## License

This repository is protected under the BSD 2-Clause license.

See LICENSE for the full license text.
