import cjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'

export default {
  entry: 'src.js',
  dest: 'public/index.js',
  format: 'es',
  plugins: [
  resolve(),
  cjs()
  ]
}
