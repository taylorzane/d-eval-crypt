window.__moduleSupport = true

import buf from './node_modules/buffer/index.js';
window.Buffer = buf.Buffer
import base65536 from './node_modules/base65536/index.js';

const hashValue = decodeURIComponent(location.hash.slice(1))
const key = document.querySelector('#key')
const textarea = document.querySelector('#text')

if (hashValue !== '') {
  const decryptKey = prompt('Enter decrypt key:')
  const decrypt = CryptoJS.AES.decrypt(base65536.decode(hashValue).toString(), decryptKey).toString(CryptoJS.enc.Utf8)
  textarea.value = decrypt
}

textarea.addEventListener('input', (event) => {
  const { value } = event.target
  const base65k = base65536.encode(new Buffer(CryptoJS.AES.encrypt(value, key.value).toString()))
  location.hash = base65k
})

const evalButton = document.querySelector('#eval')

evalButton.addEventListener('click', (event) => {
  ;(0,eval)(textarea.value)
})
